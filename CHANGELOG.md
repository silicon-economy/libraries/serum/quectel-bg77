# Changelog

## Current Development

## v0.4.0
- Update `embedded-nal` to 0.9.0

## v0.3.0
- Update to _Rust_ 1.82.0 and _nixpkgs_ 24.05
- Update to _embedded-hal_ 1.0
  - Use of embedded-timers and embedded-hal-nb
- Improve logging
- Exchange timer fields of `Bg77Hal` for single clock reference
- Add constructor function for `Bg77Hal`
- Clarify behaviour of _PWRKEY_

## v0.2.3 - 2024-10-31
- Configure `bg77` feature in `package.metadata.docs.rs` to build documentation on docs.rs

## v0.2.2
- Use `cargo-readme` for README.md generation

## v0.2.1
- First version released on crates.io
- Support for BG77 and BG770
- From the `embedded-nal` crate, `TcpClientStack`, `UdpClientStack` and `Dns::get_host_by_name` for IPv4 addresses are implemented
- Support for 12 concurrently opened sockets
