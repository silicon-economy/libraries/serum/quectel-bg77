{ nightly ? false, example ? false }:
let
  # Oxalica's Rust overlay gives us the rust-bin function which allows us
  # to select a specific Rust toolchain. Furthermore, we can configure
  # additional targets like shown below.
  rust_overlay = import
    (builtins.fetchTarball {
      name = "rust-overlay-2024-11-19";
      url = "https://github.com/oxalica/rust-overlay/archive/71287228d96e9568e1e70c6bbfa3f992d145947b.tar.gz";
      sha256 = "1sg1myrnrfyv7jn5srinrfinvbz8qzly5mmd3rshz9dl0039lh14";
    });

  # Our own overlay with additional build environment tools
  serum_overlay = import (builtins.fetchGit {
    name = "serum-overlay-2023-05-05";
    url = "https://git.openlogisticsfoundation.org/silicon-economy/libraries/serum/serum-nix-overlay.git";
    allRefs = true;
    rev = "d3781d433a1ca275c3882470dfb9b7eaeec9c0d6"; # Use the newest version here
  });

  # Pinned nixpkgs
  nixpkgsBase = import (builtins.fetchTarball {
    name = "nixpkgs-stable-24.05";
    url = "https://github.com/NixOS/nixpkgs/archive/refs/tags/24.05.tar.gz";
    # sha256 = "0000000000000000000000000000000000000000000000000000";
    sha256 = "1lr1h35prqkd1mkmzriwlpvxcb34kmhc9dnr48gkm8hh089hifmx";
  });

  nixpkgs = nixpkgsBase {
    overlays = [ rust_overlay serum_overlay ];
  };

  # Choose between a specific Rust channel and 'latest', between stable and nightly
  # For nightly, we use a specific one so we do not download a new compiler every day
  rustChannel = (
    if nightly
    then nixpkgs.rust-bin.nightly."2024-11-19"
    else nixpkgs.rust-bin.stable."1.82.0"
  ).default;

  rust = if example
    then rustChannel.override { targets = [ "thumbv7em-none-eabihf" ]; }
    else rustChannel;

  exampleDeps = if example
    then [
      nixpkgs.probe-run
    ] else [];

in
with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";

  buildInputs = [
    rust
    gitlab-clippy
    cargo2junit
    cargo-readme # generate README.md from lib.rs
    cargo-deny # license checking
    cargo-udeps
    cargo-tarpaulin
    nodePackages.cspell #  spell checking
    git
    which
    cacert # for certificate verification when downloading from crates.io
    libiconv # for linking on macOS
  ] ++ exampleDeps;

  # Set Environment Variables
  RUST_BACKTRACE = 1;
}
