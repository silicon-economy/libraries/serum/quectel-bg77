// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Unfortunately, all examples are obviously very dependent on the hardware on which they are run.
//! Because every example requires a platform setup, it makes sense to extract the required code
//! into its own module, which also allows for supporting different boards.
//!
//! Currently, the Sensing Puck (TODO link to hardware release) and Motion2SE (TODO link) are
//! supported.
//!
//! To successfully run it on a different platform, the following has to be adjusted:
//! - The pin configuration for the modem pins.
//! - The `quectel_bg77::RadioConfig` which should be used to connect to the network
//! - The APN and operator configuration for which the SIM card can be used. Alternatively, set it
//! to `None` to try automatic selection.
//! - The pin configuration for the debugging output over usart. If this should not be used, it has
//! to be removed from the `setup_logger` function in `common.rs`.
//! - The `dev-dependencies` in `Cargo.toml`: These must match your MCU, too. For example, if using
//! another stm32l4 MCU, change the `stm32l4xx-hal` features accordingly.
//! - The `.cargo/config.toml` to target the right architecture
//! - The `memory.x` file to match the memory layout of the MCU in use

pub mod common;

#[cfg(feature = "sensing_puck")]
pub mod sensing_puck;
#[cfg(feature = "sensing_puck")]
pub use sensing_puck::sensing_puck_setup as board_setup;

#[cfg(feature = "motion2se")]
pub mod motion2se;
#[cfg(feature = "motion2se")]
pub use motion2se::motion2se_setup as board_setup;
