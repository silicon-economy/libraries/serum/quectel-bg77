// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Simple implementations for whatever is needed to run the examples.
//! Do not copy and use those in production without evaluation!

pub use hal::stm32 as pac;
pub use stm32l4xx_hal as hal;

use core::{fmt::Write, sync::atomic::AtomicU32, sync::atomic::Ordering};

use rtt_target::{rprintln, rtt_init_print};

// Logger for the log crate
struct Logger;
static LOGGER: Logger = Logger;
#[cfg(feature = "sensing_puck")]
type LoggerTx = hal::serial::Tx<hal::stm32::USART2>;
#[cfg(feature = "motion2se")]
type LoggerTx = hal::serial::Tx<hal::stm32::UART4>;
static mut WRITER: Option<LoggerTx> = None;

pub fn setup_logger(debug_tx: LoggerTx) {
    rtt_init_print!();
    unsafe {
        WRITER = Some(debug_tx);
    }
    log::set_logger(&LOGGER).unwrap();
    log::set_max_level(log::LevelFilter::Trace);
}

impl log::Log for Logger {
    fn enabled(&self, _metadata: &log::Metadata) -> bool {
        true
    }

    fn log(&self, record: &log::Record) {
        if self.enabled(record.metadata()) {
            let target = record.target();
            let level = record.level();
            let args = record.args();
            //let writer = unsafe { WRITER.as_deref_mut().unwrap() };
            unsafe {
                let writer = WRITER.as_mut().unwrap();
                let _ = writeln!(writer, "[{}] {:<5} {}", target, level, args);
                rprintln!("[{}] {:<5} {}", target, level, args);
            }
        }
    }

    fn flush(&self) {}
}

// Timing (will overflow and possibly crash after ~50 days)
static SYSTEM_TICKS: AtomicU32 = AtomicU32::new(0);
pub fn increment_system_tick_1ms() {
    SYSTEM_TICKS.fetch_add(1, Ordering::Relaxed);
}
fn system_ticks() -> u32 {
    SYSTEM_TICKS.load(Ordering::Relaxed)
}

pub struct Timer {
    start: u32,
    duration: u32,
}
impl Timer {
    pub fn new() -> Self {
        Timer {
            start: 0,
            duration: 0,
        }
    }
}
impl embedded_hal::timer::CountDown for Timer {
    type Time = core::time::Duration;
    fn start<T>(&mut self, count: T)
    where
        T: Into<Self::Time>,
    {
        self.start = system_ticks();
        self.duration = count.into().as_millis() as u32;
    }
    fn wait(&mut self) -> nb::Result<(), void::Void> {
        if system_ticks() - self.start > self.duration {
            Ok(())
        } else {
            Err(nb::Error::WouldBlock)
        }
    }
}

// impl embedded_hal::serial::Read for the (stm32l4xx-hal) serial dma rx
// Unfortunately, the different hal::dma::CircBuffer instantiations are
// macro-generated, so without using macros ourselves, we can only do this
// here for hal::serial::RxDma1 = hal::dma::RxDma<Rx<USART1>, C5>.
pub struct CircBufferReader {
    pub(crate) circ_buffer: hal::dma::CircBuffer<[u8; 2048], hal::serial::RxDma1>,
}

impl embedded_hal::serial::Read<u8> for CircBufferReader {
    type Error = hal::serial::Error; // we translate from hal::dma::Error
    fn read(&mut self) -> nb::Result<u8, hal::serial::Error> {
        let mut buf = [0; 1];
        let len = self.circ_buffer.read(&mut buf).map_err(|e| {
            match e {
                hal::dma::Error::Overrun => hal::serial::Error::Overrun,
                _ => hal::serial::Error::Noise, // noise used as "other"
            }
        })?;
        match len {
            0 => Err(nb::Error::WouldBlock),
            1 => Ok(buf[0]),
            _ => panic!("We have read more than 1 byte in a 1-byte-buffer?"),
        }
    }
}
