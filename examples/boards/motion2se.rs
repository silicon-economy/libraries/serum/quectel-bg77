// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! Motion2SE board setup

pub use hal::stm32 as pac;
pub use stm32l4xx_hal as hal;

//use enumflags2::BitFlag;
use hal::{dma::CircReadDma, gpio::Output, gpio::PushPull, prelude::*, serial::Serial};

use cortex_m::singleton;
use cortex_m_rt::exception;
use panic_probe as _;

use inverted_pin::InvertedPin;

use super::common;

type PA5 = hal::gpio::PA5<Output<PushPull>>; // Enable
type PA8 = hal::gpio::PA8<Output<PushPull>>; // reset inverted
type PA11 = hal::gpio::PA11<Output<PushPull>>; // pwrkey inverted
type Usart1Tx = hal::serial::Tx<stm32l4xx_hal::pac::USART1>;

#[allow(clippy::type_complexity)]
pub fn motion2se_setup() -> core::cell::RefCell<
    quectel_bg77::Bg77Driver<
        PA5,
        InvertedPin<PA8>,
        InvertedPin<PA11>,
        Usart1Tx,
        common::CircBufferReader,
        stm32l4xx_hal::serial::Error,
        common::Timer,
    >,
> {
    let cp = cortex_m::Peripherals::take().unwrap();
    let dp = pac::Peripherals::take().unwrap();

    let mut rcc = dp.RCC.constrain();
    let mut flash = dp.FLASH.constrain();
    let mut pwr = dp.PWR.constrain(&mut rcc.apb1r1);

    let msi_range = hal::rcc::MsiFreq::RANGE2M;
    let systick_reload_val = 2000; // 1 ms systick
    let clocks = rcc.cfgr.msi(msi_range).freeze(&mut flash.acr, &mut pwr);

    let mut syst = cp.SYST;
    syst.set_clock_source(cortex_m::peripheral::syst::SystClkSource::Core);
    syst.set_reload(systick_reload_val);
    syst.clear_current();
    syst.enable_counter();
    syst.enable_interrupt();

    //
    // Init GPIO ports and DMA
    //
    let mut gpioa = dp.GPIOA.split(&mut rcc.ahb2);
    let mut gpiob = dp.GPIOB.split(&mut rcc.ahb2);
    let dma1_channels = dp.DMA1.split(&mut rcc.ahb1);

    // Power Latch on
    let mut pin_latch = gpiob
        .pb2
        .into_push_pull_output(&mut gpiob.moder, &mut gpiob.otyper);
    pin_latch.set_high();

    //
    // Init Modem
    //
    // bg770 serial
    let txpin =
        gpioa
            .pa9
            .into_alternate_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrh);
    let rxpin =
        gpioa
            .pa10
            .into_alternate_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrh);
    let serial = Serial::usart1(
        dp.USART1,
        (txpin, rxpin),
        hal::serial::Config::default().baudrate(115_200.bps()),
        clocks,
        &mut rcc.apb2,
    );
    let (bg770_tx, bg770_rx) = serial.split();
    let bg770_rxbuf = singleton!(: [u8; 2048] = [0; 2048]).unwrap();
    let bg770_rx_circ_buffer = bg770_rx.with_dma(dma1_channels.5).circ_read(bg770_rxbuf);
    let bg770_rx = common::CircBufferReader {
        circ_buffer: bg770_rx_circ_buffer,
    };
    // bg77 gpio config
    let bg770_enable = gpioa
        .pa5
        .into_push_pull_output(&mut gpioa.moder, &mut gpioa.otyper);
    let bg770_reset = gpioa
        .pa8
        .into_push_pull_output(&mut gpioa.moder, &mut gpioa.otyper);
    let bg770_reset_n = InvertedPin::new(bg770_reset);
    let bg770_pwrkey_n = gpioa
        .pa11
        .into_push_pull_output(&mut gpioa.moder, &mut gpioa.otyper);
    let bg770_pwrkey = InvertedPin::new(bg770_pwrkey_n);
    // bg77 setup
    let at_timer = common::Timer::new();
    let modem_timer = common::Timer::new();
    let bg770_hal = quectel_bg77::Bg77Hal {
        pin_enable: bg770_enable,
        pin_reset_n: bg770_reset_n,
        pin_pwrkey: bg770_pwrkey,
        tx: bg770_tx,
        rx: bg770_rx,
        at_timer,
        modem_timer,
    };
    let radio_config = quectel_bg77::RadioConfig::OnlyNbiot {
        bands: quectel_bg77::NbiotBand::B8.into(),
    };
    /* Alternatively, something like:
    let radio_config = quectel_bg77::RadioConfig::Prioritized {
        preference: quectel_bg77::RadioTechnology::NB_IoT,
        emtc_bands: quectel_bg77::EmtcBand::all(),
        nbiot_bands: quectel_bg77::NbiotBand::all(),
    };
    */
    let bg770 = quectel_bg77::Bg77Driver::new(
        bg770_hal,
        radio_config,
        //None,
        //None,
        Some("iot.1nce.net"),
        Some("26201"),
        core::time::Duration::from_secs(60),
        core::time::Duration::from_millis(500),
        core::time::Duration::from_millis(20),
    );

    //
    // Init debug output
    //
    let txpin =
        gpioa
            .pa0
            .into_alternate_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);
    let rxpin =
        gpioa
            .pa1
            .into_alternate_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrl);
    let serial = Serial::uart4(
        dp.UART4,
        (txpin, rxpin),
        hal::serial::Config::default().baudrate(115_200.bps()),
        clocks,
        &mut rcc.apb1r1,
    );
    let (debug_tx, mut _debug_rx) = serial.split();

    // Init log crate
    common::setup_logger(debug_tx);

    bg770
}

#[exception]
#[allow(non_snake_case)]
fn SysTick() {
    common::increment_system_tick_1ms();
}
