// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

//! DNS example
//!
//! This example demonstrates how to do DNS resolutions.
//!
//! This example will print some output over serial uart and RTT.

#![no_std]
#![no_main]

pub use hal::stm32 as pac;
pub use stm32l4xx_hal as hal;

use panic_probe as _;

mod boards;

#[cortex_m_rt::entry]
fn main() -> ! {
    let mut driver = boards::board_setup();

    let stack = quectel_bg77::Bg77ClientStack::new(&mut driver);

    if let Err(e) = dns_example(stack) {
        log::error!("Example failed: {:?}", e);
    } else {
        log::info!("Example successful");
    }

    #[allow(clippy::empty_loop)]
    loop {}
}

fn dns_example<E, BG77>(mut bg77: BG77) -> Result<(), E>
where
    E: core::fmt::Debug,
    BG77: embedded_nal::Dns<Error = E>,
{
    let domain = "rust-lang.org";
    let address = nb::block!(bg77.get_host_by_name(domain, embedded_nal::AddrType::IPv4))?;
    log::info!("{}'s address is: {}", domain, address);
    Ok(())
}
