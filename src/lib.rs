// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

#![cfg_attr(not(test), no_std)]

//! This crate implements a driver for the Quectel BG77 and BG770 eMTC and NB-IoT modems using the
//! `embedded-hal` traits for the underlying hardware abstractions and implementing the `embedded-nal`
//! traits for users of this library. Currently, `TcpClientStack` and `UdpClientStack` are supported
//! for both modems. From the `Dns` trait, only the `get_host_by_name` requesting only IPv4 addresses
//! is supported. Thus far, this has only been working for BG77. To select which modem is used,
//! activate either the `bg77` or the `bg770` feature.
//!
//! The driver supports up to 12 sockets at the same time and implements `Drop`/RAII on the socket
//! handle types to prevent resource leaks. This is accomplished by using the
//! [interior mutability](https://doc.rust-lang.org/book/ch15-05-interior-mutability.html) pattern:
//! All socket handles hold a reference to the driver type which owns the hardware. Whenever they need
//! to access the hardware, the driver is mutably borrowed (checked at runtime). This always works
//! because no mutable borrow outlives any method call.
//!
//! Currently, AT commands are not run partially which would complicate the interior state handling
//! and increase the number of possible error causes, e.g. when multiple sockets try to access the
//! modem at the same time. As a consequence, all method calls are effectively blocking. The only
//! exception is `receive` which returns `nb::Error::WouldBlock` if no data at all is available. For
//! the `send` method, this is not a huge problem because it only blocks until the data is transferred
//! to the modem (the actual transmission happens in the background then). Only connecting the modem
//! to the network blocks for a long time.
//!
//! # Usage Example
//! ```no_run
//! use embedded_nal::TcpClientStack;
//!
//! // The hardware abstraction used for the Bg77Hal must implement the respective embedded-hal
//! // traits
//! # use embedded_hal_mock::*;
//! # use embedded_timers::timer::Timer;
//! # use core::convert::Infallible;
//! # struct MockPin(bool);
//! # impl embedded_hal::digital::ErrorType for MockPin {
//! #     type Error = Infallible;
//! # }
//! # impl embedded_hal::digital::OutputPin for MockPin {
//! #     fn set_low(&mut self) -> Result<(), Self::Error> {
//! #         self.0 = false;
//! #         Ok(())
//! #     }
//! #     fn set_high(&mut self) -> Result<(), Self::Error> {
//! #         self.0 = true;
//! #         Ok(())
//! #     }
//! # }
//! # struct MockTx(std::collections::VecDeque<u8>);
//! # struct MockRx(std::collections::VecDeque<u8>);
//! # impl embedded_hal_nb::serial::ErrorType for MockTx {
//! #     type Error = Infallible;
//! # }
//! # impl embedded_hal_nb::serial::Write<u8> for MockTx {
//! #     fn write(&mut self, ch: u8) -> Result<(), nb::Error<Infallible>> {
//! #         self.0.push_back(ch);
//! #         Ok(())
//! #     }
//! #     fn flush(&mut self) -> Result<(), nb::Error<Infallible>> {
//! #         Ok(())
//! #     }
//! # }
//! # impl embedded_hal_nb::serial::ErrorType for MockRx {
//! #     type Error = Infallible;
//! # }
//! # impl embedded_hal_nb::serial::Read<u8> for MockRx {
//! #     fn read(&mut self) -> Result<u8, nb::Error<Infallible>> {
//! #         match self.0.pop_front() {
//! #             Some(ch) => Ok(ch),
//! #             None => Err(nb::Error::WouldBlock),
//! #         }
//! #     }
//! # }
//! # let bg77_enable = MockPin(false);
//! # let bg77_reset_n = MockPin(true);
//! # let bg77_pwrkey = MockPin(true);
//! # let bg77_tx = MockTx(std::collections::VecDeque::new());
//! # let bg77_rx = MockRx(std::collections::VecDeque::new());
//! # struct Clock;
//! # impl embedded_timers::clock::Clock for Clock {
//! #     type Instant = std::time::Instant;
//! #     fn now(&self) -> Self::Instant {
//! #         std::time::Instant::now()
//! #     }
//! # }
//! let bg77_hal = quectel_bg77::Bg77Hal::new(
//!     bg77_enable,
//!     bg77_reset_n,
//!     bg77_pwrkey,
//!     bg77_tx,
//!     bg77_rx,
//!     &Clock,
//! );
//! // choose which radio technologies and bands to use
//! let radio_config = quectel_bg77::RadioConfig::OnlyNbiot {
//!     bands: quectel_bg77::NbiotBand::B8.into(),
//! };
//! let mut bg77 = quectel_bg77::Bg77Driver::new(
//!     bg77_hal,
//!     radio_config,
//!     Some("iot.1nce.net"),                   // configure APN
//!     Some("26201"),                          // configure operator
//!     core::time::Duration::from_secs(60),    // configure connection/attach timeout
//!     core::time::Duration::from_millis(500), // configure internal safety delays
//!     core::time::Duration::from_millis(20),  // configure internal safety delays
//! );
//! let mut bg77 = quectel_bg77::Bg77ClientStack::new(&mut bg77);
//! // request a new socket handle, this only fails if all sockets are already in use
//! let mut socket = bg77.socket().unwrap();
//! // turn on the modem and try to attach to the network; generally, this takes the most time
//! let socket_address: core::net::SocketAddr = "192.168.1.1:8080".parse().unwrap();
//! nb::block!(bg77.connect(&mut socket, socket_address))?;
//! // transmit data via the socket
//! nb::block!(bg77.send(&mut socket, b"Hello, BG77"))?;
//! // close the socket; when the last socket is closed, this also turns off the modem
//! // with this driver, this never fails
//! bg77.close(socket).unwrap();
//! # Ok::<(), quectel_bg77::Bg77Error>(())
//! ```
//!
//! # Examples
//! > ❗ __Note:__ Examples are currently broken.
//! > The used stm32 HAL does not support _embedded-hal_ `1.0` and needs to be replaced.
//!
//! There are a few examples which can be run on appropriate hardware. This driver was initially
//! developed alongside the
//! [Sensing Puck](https://www.silicon-economy.com/neue-open-source-hardware-sensing-puck-laesst-sich-einfach-integrieren/)
//! so this was the only supported board in the beginning. Later, support for MotionAI was added. The
//! board in use can be selected by activating either the `sensing_puck` or the `motion2se` feature
//! which will automatically activate the right modem feature (`bg77` or `bg770`).
//!
//! If required, more boards can be added under `examples/boards` with appropriate features in
//! `Cargo.toml`. Still, the `memory.x` file has to be adjusted and possibly the build target,
//! depending on the MCU architecture.
//!
//! Since only STM32L452-based boards are supported at the moment, an appropriate `memory.x` is
//! shipped. Therefore, running the examples should be as easy as, e.g.:
//! `cargo run --example tcp-client --features sensing_puck --target thumbv7em-none-eabihf`. This
//! uses `probe-run` to flash the firmware.
//!
//! # Unit tests
//! Since the examples only build for the appropriate architecture, the tests need to be run with
//! the `--lib` flag: `cargo test --lib`
//!
//! # Features
//! - `bg77` / `bg770` selects the modem in use
//! - `sensing_puck` / `motion2se` selects the board for the examples
//! - `direct-serial-access` Enables direct access to the underlying serial port. This should be
//!   used for development/debugging only.
//!
//! # License
//! Open Logistics Foundation License\
//! Version 1.3, January 2023
//!
//! See the LICENSE file in the top-level directory.
//!
//! # Contact
//! Fraunhofer IML Embedded Rust Group - <embedded-rust@iml.fraunhofer.de>

#[cfg(not(any(feature = "bg77", feature = "bg770")))]
compile_error!("A modem must be selected by enabling any of the features 'bg77' or 'bg770'");

#[cfg(all(feature = "bg77", feature = "bg770"))]
compile_error!("Only a single modem feature may be enabled");

#[macro_use(block)]
extern crate nb;

use core::net;
use core::time::Duration;

mod error;
use embedded_hal::digital::OutputPin;
use embedded_hal_nb::serial::{Read, Write};
use embedded_timers::clock::Clock;
pub use error::Bg77Error;

mod atst_commands;
mod atst_flows;
use atst_flows::Bg77CmdFlows;
pub use atst_flows::{EmtcBand, NbiotBand, RadioConfig, RadioTechnology};
use types::Protocol;

/// Hardware abstraction for the BG77
///
/// - `pin_enable` is used to physically power-up and power-down the modem, e.g. via a FET. If this
///   feature should not be used, just mock the pin :)
/// - `pin_reset_n` and `pin_pwrkey` correspond to the respective BG77 input pins. If those are
///   inverted on your PCB (with an n-channel or npn transistor), do not forget to invert the
///   software pins, too.
///   
///   __Note:__ `pin_pwrkey` is referred to as just _PWRKEY_ in the Quectel documentation although it is
///   an active-low pin which is pulled up internally.
///   
/// - `tx` and `rx` correspond to the serial uart which connects to the BG77. It is advisable that
///   `rx` is backed by a sufficiently large buffer because depending on your setting (MCU speed,
///   optimization level, type of input buffering), data handling could be slower than the physical
///   data transmission. If so, the input buffer will run quite full when receiving large chunks of
///   data. Concretely: Choose an input buffer size of at least 1460 bytes plus safety margin, 1460
///   bytes is the biggest data chunk that can be received/read in a single AT command.
/// - Timers `at_timer` and `modem_timer` for delays and timeouts
pub struct Bg77Hal<'a, PIN0, PIN1, PIN2, TX, RX, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    TX: Write<u8>,
    RX: Read<u8>,
    CLOCK: Clock,
{
    pub pin_enable: PIN0,
    pub pin_reset_n: PIN1,
    pub pin_pwrkey: PIN2,
    pub tx: TX,
    pub rx: RX,
    pub clock: &'a CLOCK,
}
impl<'a, PIN0, PIN1, PIN2, TX, RX, CLOCK> Bg77Hal<'a, PIN0, PIN1, PIN2, TX, RX, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    TX: Write<u8>,
    RX: Read<u8>,
    CLOCK: Clock,
{
    /// Create a new BG77 hardware abstraction
    ///
    /// Convenience method.
    pub fn new(
        pin_enable: PIN0,
        pin_reset_n: PIN1,
        pin_pwrkey_n: PIN2,
        tx: TX,
        rx: RX,
        clock: &'a CLOCK,
    ) -> Self {
        Self {
            pin_enable,
            pin_reset_n,
            pin_pwrkey: pin_pwrkey_n,
            tx,
            rx,
            clock,
        }
    }
}

#[derive(Clone, Copy)]
struct Bg77Socket {
    state: SocketState,
}

#[derive(Clone, Copy, PartialEq)]
enum SocketState {
    Available,
    Used,
    Open, // close -> Available again
}

impl Default for Bg77Socket {
    fn default() -> Bg77Socket {
        Bg77Socket {
            state: SocketState::Available,
        }
    }
}

#[derive(PartialEq)]
enum ModemState {
    PoweredOff,
    PoweredOnAndConnected, // currently, we do not support powering on without being connected
}

const NUM_SOCKETS: usize = 12;

/// Driver type which owns the hardware
///
/// The [`Bg77ClientStack`] will use references to this type to implement the interior mutability
/// pattern. If this shall be instantiated in a function (e.g. in a BSP), this type has to be
/// passed up the call stack before the [`Bg77ClientStack`] can be created because these references
/// must live long enough.
pub struct Bg77Driver<'a, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    flows: Bg77CmdFlows<'a, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>,
    modem_state: ModemState,
    sockets: [Bg77Socket; NUM_SOCKETS],
}

impl<'a, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
    Bg77Driver<'a, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    /// Creates a new `Bg77Driver` instance
    ///
    /// - `hal` supplies the necessary hardware abstraction to interact with the modem
    /// - `radio_config` configures how to prioritize radio technologies and LTE radio bands
    /// - `apn` can be used to set the APN manually. If not set, automatic selection will be
    ///   performed.
    /// - `operator` can be used to choose the operator manually. If not set, automatic selection
    ///   will be performed. This has to be supplied in the numeric format, i.e. the GSM location
    ///   area identification number, e.g. "26201" for German Telekom.
    /// - `connection_timeout` specifies how much patience we have for the connection/attachment
    ///   procedure. Other AT command timings are currently not configurable but chosen to use
    ///   sensible defaults for a low-power WAN like eMTC or NB-IoT. These are mostly in the order of
    ///   some seconds.
    /// - `delay_before_attach` is a `Duration` that will be waited before the `AT+COPS` command
    ///   is run to connect/attach to the network. This has been introduced because we have
    ///   experienced connection issues on a low-power device when doing this too fast. In our case,
    ///   a delay of 500 ms helped but this could/should be avoided on devices where it is not
    ///   necessary.
    /// - `tx_rx_cooldown` is a `Duration` that will be waited before switching between transmitting
    ///   to and receiving from the modem over the serial uart. This has been introduced because
    ///   we have experienced stability issues when doing this too fast. In our case, a delay of
    ///   20 ms was sufficient, this does not delay the whole process too much. Anyways, it could
    ///   also be avoided when it is not necessary.
    pub fn new(
        mut hal: Bg77Hal<'a, PIN0, PIN1, PIN2, TX, RX, CLOCK>,
        radio_config: RadioConfig,
        apn: Option<&str>,
        operator: Option<&str>,
        connection_timeout: Duration,
        delay_before_attach: Duration,
        tx_rx_cooldown: Duration,
    ) -> core::cell::RefCell<Self> {
        // alternative: pub fn new(...) -> core::cell::RefCell<Bg77Driver<PIN0, PIN1, PIN2, TX, RX, UartErr, TIMER>> {
        // Bring hardware into well-defined state
        let _ = hal.pin_enable.set_low();
        let _ = hal.pin_reset_n.set_high();
        let _ = hal.pin_pwrkey.set_high();

        let driver = Bg77Driver {
            flows: Bg77CmdFlows::new(
                hal,
                radio_config,
                apn,
                operator,
                connection_timeout,
                delay_before_attach,
                tx_rx_cooldown,
            ),
            modem_state: ModemState::PoweredOff,
            sockets: [Bg77Socket::default(); NUM_SOCKETS],
        };
        core::cell::RefCell::new(driver)
    }

    /// Turns on the modem and tries to connect to the network
    ///
    /// This method is called automatically whenever a socket connection should be established.
    /// Thus, this method does not need to be called manually.
    pub fn power_on_and_connect_network(&mut self) -> Result<(), Bg77Error> {
        match self.modem_state {
            ModemState::PoweredOnAndConnected => return Ok(()),
            ModemState::PoweredOff => {}
        }
        self.flows.power_on()?;
        let res = self.flows.connect_network();
        if res.is_err() {
            log::warn!("Could not connect to network, powering modem off.");
            self.flows.power_off();
            return res;
        }
        self.modem_state = ModemState::PoweredOnAndConnected;
        Ok(())
    }

    /// Checks if all sockets are closed. If so, tries to disconnect/detach from the network and
    /// powers down the modem.
    fn disconnect_network_and_power_down(&mut self) {
        match self.modem_state {
            ModemState::PoweredOff => return,
            ModemState::PoweredOnAndConnected => {}
        }
        for i in 0..NUM_SOCKETS {
            if let SocketState::Open = self.sockets[i].state {
                return;
            }
        }
        if self.flows.disconnect_network().is_err() {
            log::warn!("Could not detach from network, powering down anyways.");
        }
        self.flows.power_off();
        self.modem_state = ModemState::PoweredOff;
    }

    // The following methods implement the embedded_nal::Tcp-/UdpClientStack methods.
    // They differ in the following details:
    // - sockets are identified by index into the sockets array instead of the socket handle
    // - for some methods, the sockets require an additional protocol specification

    fn socket(&mut self) -> Result<usize, Bg77Error> {
        for i in 0..NUM_SOCKETS {
            if let SocketState::Available = self.sockets[i].state {
                self.sockets[i].state = SocketState::Used;
                return Ok(i);
            }
        }
        Err(Bg77Error::NoSocketAvailable)
    }

    fn connect(
        &mut self,
        socket_index: usize,
        protocol: Protocol,
        remote: net::SocketAddr,
    ) -> Result<(), Bg77Error> {
        match self.sockets[socket_index].state {
            SocketState::Available => panic!("Socket currently being used is 'Available'?"),
            SocketState::Used => {}
            SocketState::Open => return Err(Bg77Error::SocketAlreadyConnected),
        }
        self.power_on_and_connect_network()?;
        let res = self.flows.open_socket(socket_index, protocol, remote);
        if res.is_err() {
            log::warn!("Could not open socket, checking if modem can be powered down.");
            self.disconnect_network_and_power_down();
            return res;
        }
        self.sockets[socket_index].state = SocketState::Open;
        Ok(())
    }

    fn close(&mut self, socket_index: usize) {
        let is_open = match self.sockets[socket_index].state {
            SocketState::Available => panic!("Socket currently being used is 'Available'?"),
            SocketState::Used => false,
            SocketState::Open => true,
        };
        self.sockets[socket_index].state = SocketState::Available;
        if is_open && self.flows.close_socket(socket_index).is_err() {
            log::warn!("Could not close socket. Assuming the socket is closed anyways.");
        }
        // power off after the last socket has been closed
        for i in 0..NUM_SOCKETS {
            if let SocketState::Available = self.sockets[i].state {
            } else {
                return; // at least one more socket in use
            }
        }
        self.disconnect_network_and_power_down();
    }

    fn send(
        &mut self,
        socket_index: usize,
        protocol: Protocol,
        data: &[u8],
    ) -> Result<usize, Bg77Error> {
        match self.sockets[socket_index].state {
            SocketState::Available => panic!("Socket currently being used is 'Available'?"),
            SocketState::Used => return Err(Bg77Error::NotConnected),
            SocketState::Open => {}
        }
        self.flows.send_data(socket_index, protocol, data)
    }

    fn receive(
        &mut self,
        socket_index: usize,
        buffer: &mut [u8],
    ) -> Result<usize, nb::Error<Bg77Error>> {
        match self.sockets[socket_index].state {
            SocketState::Available => panic!("Socket currently being used is 'Available'?"),
            SocketState::Used => return Err(nb::Error::Other(Bg77Error::NotConnected)),
            SocketState::Open => {}
        }
        // TODO Move to a better doc place. Leave only the note that flows.rx_data() is safe.
        // This method does not really support async rx. It only returns WouldBlock when there is
        // no data at all available currently. With the current approach, async operation is
        // impossible because the driver borrow never outlives the method calls. So a single
        // transaction can not safely be split on multiple method calls!
        self.flows.receive_data(socket_index, buffer)
    }

    fn get_host_by_name(&mut self, hostname: &str) -> Result<net::IpAddr, Bg77Error> {
        // TODO If the modem was not powered on before, we leave it on because it is most likely
        // that the user wants to use that IP address for a socket they connect afterwards. If this
        // is not what they do, the modem will stay powered on which is probably not what they want
        // either.
        let powered_on_before = self.modem_state == ModemState::PoweredOnAndConnected;
        self.power_on_and_connect_network()?;
        let dns = self.flows.dns_lookup(hostname);
        if let (&Err(_), false) = (&dns, powered_on_before) {
            self.disconnect_network_and_power_down();
        }
        dns
    }

    /// Gives access to the underlying serial
    ///
    /// This method is marked as `unsafe` because this could misconfigure the modem and leave it in
    /// an unexpected state. This can cause the modem driver to fail in unexpected ways.
    #[cfg(feature = "direct-serial-access")]
    pub unsafe fn serial(&mut self) -> (&mut TX, &mut RX) {
        self.flows.serial()
    }
}

/// The BG77 main type, called `ClientStack` following the [`embedded_nal`] naming scheme
///
/// Implements [`embedded_nal::TcpClientStack`], [`embedded_nal::UdpClientStack`] and
/// [`embedded_nal::Dns::get_host_by_name`].
///
/// This uses references to [`Bg77Driver`] to implement the interior mutability pattern.
///
/// Powering on the modem is handled automatically whenever it is required, i.e. when
/// a socket should be connected or a dns resolution should be performed. Likewise, the
/// modem is turned off when the last socket is closed. After a dns resolution, the modem stays
/// powered on because it is assumed that most users want to establish a connection then. If this
/// is not what you want, acquire a socket and drop/close it immediately. This will check if the
/// modem can be powered off, which is the case if there are no other open sockets.
///
/// The socket handles implement `Drop` to automatically close the socket when they are dropped to
/// prevent resource leaks.
pub struct Bg77ClientStack<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    driver: &'a core::cell::RefCell<Bg77Driver<'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>>,
}

impl<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
    Bg77ClientStack<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    /// Although we only store the Bg77Driver as a `&RefCell` we require a `&mut RefCell`. Like
    /// this, we can be sure that there are no other `&RefCell`s to the driver, only the ones we
    /// control ourselves. This ensures that `driver.borrow_mut()` will never panic because:
    /// 1. Our own `borrow_mut()` calls will never outlive the method calls.
    /// 2. The network stack and the sockets can not be transferred to other threads/contexts (i.e.
    ///    they are `!Send + !Sync`).
    pub fn new(
        driver: &'a mut core::cell::RefCell<
            Bg77Driver<'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>,
        >,
    ) -> Self {
        Bg77ClientStack { driver }
    }
}

/// Opaque socket handle type for a TCP socket
///
/// Implements `Drop` to automatically close the socket when it is dropped.
pub struct Bg77TcpSocketHandle<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    index: usize,
    driver: &'a core::cell::RefCell<Bg77Driver<'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>>,
}

impl<PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK> Drop
    for Bg77TcpSocketHandle<'_, '_, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    fn drop(&mut self) {
        self.driver.borrow_mut().close(self.index)
    }
}

/// Opaque socket handle type for a UDP socket
///
/// Implements `Drop` to automatically close the socket when it is dropped.
pub struct Bg77UdpSocketHandle<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    index: usize,
    driver: &'a core::cell::RefCell<Bg77Driver<'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>>,
    /// embedded_nal::UdpClientStack::receive also returns a SocketAddr but the AT+QIRD command
    /// does not tell us where it received data from. So we can just assume that it came from the
    /// remote we "connected" to before (in other contexts, UDP sockets are not considered
    /// "connected" at all but just send to a remote address).
    remote: Option<net::SocketAddr>,
}

impl<PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK> Drop
    for Bg77UdpSocketHandle<'_, '_, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    fn drop(&mut self) {
        self.driver.borrow_mut().close(self.index)
    }
}

impl<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK> embedded_nal::TcpClientStack
    for Bg77ClientStack<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    type TcpSocket = Bg77TcpSocketHandle<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>;
    type Error = Bg77Error;

    fn socket(&mut self) -> Result<Self::TcpSocket, Self::Error> {
        Ok(Bg77TcpSocketHandle {
            index: self.driver.borrow_mut().socket()?,
            driver: self.driver,
        })
    }

    fn connect(
        &mut self,
        socket: &mut Self::TcpSocket,
        remote: net::SocketAddr,
    ) -> Result<(), nb::Error<Self::Error>> {
        let mut driver = self.driver.borrow_mut();
        Ok(driver.connect(socket.index, Protocol::Tcp, remote)?)
    }

    fn send(
        &mut self,
        socket: &mut Self::TcpSocket,
        data: &[u8],
    ) -> Result<usize, nb::Error<Self::Error>> {
        let tx_len = self
            .driver
            .borrow_mut()
            .send(socket.index, Protocol::Tcp, data)?;
        Ok(tx_len)
    }

    fn receive(
        &mut self,
        socket: &mut Self::TcpSocket,
        buffer: &mut [u8],
    ) -> Result<usize, nb::Error<Self::Error>> {
        self.driver.borrow_mut().receive(socket.index, buffer)
    }

    fn close(&mut self, _socket: Self::TcpSocket) -> Result<(), Self::Error> {
        // Do not do anything here!!!
        // The `Bg77TcpSocketHandle` implements Drop, so `Bg77Driver::close(socket.index)` will be
        // called when the socket handle goes out of scope (at the end of this method). If we
        // called `Bg77Driver::close(...)` here, it would be called twice, which is bad.
        Ok(())
    }
}

impl<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK> embedded_nal::UdpClientStack
    for Bg77ClientStack<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    type UdpSocket = Bg77UdpSocketHandle<'a, 'b, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>;
    type Error = Bg77Error;

    fn socket(&mut self) -> Result<Self::UdpSocket, Self::Error> {
        Ok(Bg77UdpSocketHandle {
            index: self.driver.borrow_mut().socket()?,
            driver: self.driver,
            remote: None,
        })
    }

    fn connect(
        &mut self,
        socket: &mut Self::UdpSocket,
        remote: net::SocketAddr,
    ) -> Result<(), Self::Error> {
        let mut driver = self.driver.borrow_mut();
        driver.connect(socket.index, Protocol::Udp, remote)?;
        socket.remote = Some(remote);
        Ok(())
    }

    fn send(
        &mut self,
        socket: &mut Self::UdpSocket,
        data: &[u8],
    ) -> Result<(), nb::Error<Self::Error>> {
        self.driver
            .borrow_mut()
            .send(socket.index, Protocol::Udp, data)?;
        Ok(())
    }

    fn receive(
        &mut self,
        socket: &mut Self::UdpSocket,
        buffer: &mut [u8],
    ) -> Result<(usize, net::SocketAddr), nb::Error<Self::Error>> {
        let rx_len = self.driver.borrow_mut().receive(socket.index, buffer)?;
        let remote = socket
            .remote
            .expect("remote should be set when we are connected");
        Ok((rx_len, remote))
    }

    fn close(&mut self, _socket: Self::UdpSocket) -> Result<(), Self::Error> {
        // Do not do anything here (see TcpClientStack impl)!!!
        Ok(())
    }
}

impl<PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK> embedded_nal::Dns
    for Bg77ClientStack<'_, '_, PIN0, PIN1, PIN2, TX, RX, UartErr, CLOCK>
where
    PIN0: OutputPin,
    PIN1: OutputPin,
    PIN2: OutputPin,
    UartErr: core::fmt::Debug,
    TX: Write<u8, Error = UartErr>,
    RX: Read<u8, Error = UartErr>,
    CLOCK: Clock,
{
    type Error = Bg77Error;

    /// Resolve the first ip address of a host, given its hostname and a desired address record
    /// type to look for
    ///
    /// In the BG77's implementation, there are a few things to consider currently:
    /// - Only IPv4 supported
    /// - If required, the modem is powered on. Because it is probable that the user wants to use
    ///   the resolved IP address, the modem is not turned off afterwards if the DNS lookup is
    ///   successful. To force powering down the modem, open and close a socket afterwards.
    fn get_host_by_name(
        &mut self,
        hostname: &str,
        addr_type: embedded_nal::AddrType,
    ) -> nb::Result<net::IpAddr, Self::Error> {
        if let embedded_nal::AddrType::IPv6 = addr_type {
            Err(nb::Error::Other(Bg77Error::IPv6Unsupported))
        } else {
            Ok(self.driver.borrow_mut().get_host_by_name(hostname)?)
        }
    }

    /// Not implemented, always returns `Err`
    fn get_host_by_address(
        &mut self,
        _addr: net::IpAddr,
        _result: &mut [u8],
    ) -> nb::Result<usize, Self::Error> {
        Err(nb::Error::Other(Bg77Error::ReverseDnsUnsupported))
    }
}

mod types {
    #[derive(PartialEq, Eq)]
    pub enum Protocol {
        Udp,
        Tcp,
    }
}

mod util {
    use core::time::Duration;

    use embedded_timers::{clock::Clock, timer::Timer};
    pub fn wait_ms<CLOCK: Clock>(timer: &mut Timer<'_, CLOCK>, millis: u64) {
        wait(timer, Duration::from_millis(millis));
    }

    pub fn wait<CLOCK: Clock>(timer: &mut Timer<'_, CLOCK>, duration: Duration) {
        timer.start(duration);
        block!(timer.wait())
            .expect("Timer::wait() should not fail after the timer has been started");
    }
}

#[cfg(test)]
mod tests {
    use core::convert::Infallible;
    use core::net;
    use embedded_nal::TcpClientStack;
    use embedded_timers::instant::Instant64;
    use std::time;

    struct MockPin(bool);
    struct MockTx(std::collections::VecDeque<u8>);
    struct MockRx(std::collections::VecDeque<u8>);
    struct FastStdClock {
        start: time::Instant,
    }

    impl FastStdClock {
        fn new() -> Self {
            FastStdClock {
                start: time::Instant::now(),
            }
        }
    }

    impl embedded_timers::clock::Clock for FastStdClock {
        type Instant = Instant64<1_000_000_000>;

        fn now(&self) -> Self::Instant {
            let ticks = time::Instant::now().duration_since(self.start).as_nanos() as u64;
            Instant64::new(ticks * 100)
        }
    }

    impl embedded_hal::digital::ErrorType for MockPin {
        type Error = Infallible;
    }
    impl embedded_hal::digital::OutputPin for MockPin {
        fn set_low(&mut self) -> Result<(), Self::Error> {
            self.0 = false;
            Ok(())
        }
        fn set_high(&mut self) -> Result<(), Self::Error> {
            self.0 = true;
            Ok(())
        }
    }

    impl embedded_hal_nb::serial::ErrorType for MockTx {
        type Error = Infallible;
    }
    impl embedded_hal_nb::serial::Write<u8> for MockTx {
        fn write(&mut self, ch: u8) -> Result<(), nb::Error<Infallible>> {
            self.0.push_back(ch);
            Ok(())
        }
        fn flush(&mut self) -> Result<(), nb::Error<Infallible>> {
            Ok(())
        }
    }

    impl embedded_hal_nb::serial::ErrorType for MockRx {
        type Error = Infallible;
    }
    impl embedded_hal_nb::serial::Read<u8> for MockRx {
        fn read(&mut self) -> Result<u8, nb::Error<Infallible>> {
            match self.0.pop_front() {
                Some(ch) => Ok(ch),
                None => Err(nb::Error::WouldBlock),
            }
        }
    }

    #[test]
    fn failed_connect() {
        let hal = super::Bg77Hal {
            pin_enable: MockPin(false),
            pin_reset_n: MockPin(true),
            pin_pwrkey: MockPin(true),
            tx: MockTx(std::collections::VecDeque::new()),
            rx: MockRx(std::collections::VecDeque::new()),
            clock: &FastStdClock::new(),
        };
        let radio_config = super::RadioConfig::OnlyNbiot {
            bands: super::NbiotBand::B8.into(),
        };
        let mut bg77 = super::Bg77Driver::new(
            hal,
            radio_config,
            None,
            None,
            core::time::Duration::from_secs(60),
            core::time::Duration::from_millis(500),
            core::time::Duration::from_millis(20),
        );
        let mut bg77 = super::Bg77ClientStack::new(&mut bg77);
        let mut socket = bg77
            .socket()
            .expect("It should be possible to create a TCP socket");
        let v4 = net::SocketAddrV4::new(net::Ipv4Addr::new(0, 0, 0, 0), 0);
        assert_eq!(
            bg77.connect(&mut socket, net::SocketAddr::V4(v4)),
            Err(nb::Error::Other(crate::Bg77Error::AtCmdTimeout))
        );
    }
}
