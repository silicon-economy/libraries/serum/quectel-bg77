// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/// BG77 driver error type
#[derive(Debug, PartialEq, Eq)]
pub enum Bg77Error {
    /// All sockets in use
    NoSocketAvailable,
    /// It looks like a pin is required to unlock the SIM card.
    PinRequired,
    /// Internal error: Even though the operator selection succeeded, the modem
    /// does not look connected afterwards.
    CgattFailure,
    /// Trying to connect a socket that is already connected
    SocketAlreadyConnected,
    /// Could not open socket (modem did not answer, answer could not be parsed
    /// or modem answered with an unexpected message).
    OpenSocketError,
    /// An IPv6 IP address was used but IPv6 is not supported (yet?). This
    /// error originates from the fact that we use the (external)
    /// `embedded-nal::<Protocol>ClientStack` traits which accept both IPv4
    /// and IPv6 addresses so we can not prevent this kind of error statically.
    IPv6Unsupported,
    /// Trying to send or receive data when the socket is not connected.
    NotConnected,
    /// An error occurred when accessing the underlying hardware (gpio or serial)
    Hardware,
    /// An AT command timed out
    AtCmdTimeout,
    /// The modem responded to an AT command with "ERROR"
    AtCmdError,
    /// The modem responded to an AT command with "+CME ERROR: ..."
    AtCmdCmeError,
    /// An internal buffer was not big enough to handle the amount of data
    BufferOverflow,
    /// The tx data size exceeds the maximum supported tx data size (1460 bytes)
    TxSize,
    /// `embedded_nal::Dns::get_host_by_address()` was called which is not supported
    ReverseDnsUnsupported,
    /// The resolved IP address could not be parsed into a `embedded_nal::IpAddr`
    ParseIpAddressFailed,
    /// A value could not be parsed into a specific type, e.g. the modem answer (bytes) could not
    /// be parsed into an integer.
    ParseFailed,
    /// The modem returned more rx data than the maximum amount of data we requested / can handle.
    RxSize,
}

impl embedded_nal::TcpError for Bg77Error {
    fn kind(&self) -> embedded_nal::TcpErrorKind {
        // TODO Identify `PipeClosed` cases to return a more specific error if possible
        embedded_nal::TcpErrorKind::Other
    }
}

/* TODO conflicting impl
impl core::convert::Into<nb::Error<Bg77Error>> for Bg77Error {
    fn into(self) -> nb::Error<Bg77Error> {
        nb::Error::Other(self)
    }
}
*/
