// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use core::fmt::Write;
use core::net;
use core::time::Duration;
use embedded_timers::{clock::Clock, timer::Timer};
use enumflags2::{bitflags, BitFlags};
use heapless::String;

use crate::atst_commands;
use crate::error::Bg77Error;
use crate::types::Protocol;
use crate::util::{wait, wait_ms};
use crate::Bg77Hal;

/// heapless version of macro format!(...), requires a type annotation so the size of the
/// String can be determined.
/// let text: String<64> = hl_format!("{} {}", 1, "heapless::String").expect("Could not format heapless::String");
macro_rules! hl_format {
    ($dst:expr, $($arg : tt) *) => {{
        let mut formatted = String::new();
        match write!(formatted, $dst, $($arg)*) {
            Ok(_) => Ok(formatted),
            Err(_) => Err(Bg77Error::BufferOverflow),
        }
    }}
}

/// Configures which radio technologies should be used and how they should be prioritized and which
/// LTE radio bands should be used
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum RadioConfig {
    /// Uses both radio technologies with the given radio bands without specifying which one to
    /// prefer
    Both {
        emtc_bands: BitFlags<EmtcBand>,
        nbiot_bands: BitFlags<NbiotBand>,
    },
    /// Only tries e-MTC with the given radio bands
    OnlyEmtc { bands: BitFlags<EmtcBand> },
    /// Only tries NB-IoT with the given radio bands
    OnlyNbiot { bands: BitFlags<NbiotBand> },
    /// Uses both radio technologies with the given radio bands and prefers the given technology
    Prioritized {
        preference: RadioTechnology,
        emtc_bands: BitFlags<EmtcBand>,
        nbiot_bands: BitFlags<NbiotBand>,
    },
}

/// An LPWAN radio technology, eMTC or NB-IoT
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
#[allow(non_camel_case_types)]
pub enum RadioTechnology {
    NB_IoT,
    eMTC,
}

/// An e-MTC radio band
///
/// Can be used as a [`BitFlags<EmtcBand>`] from the [`enumflags2`] crate to specify a list of
/// enabled radio bands, e.g. `let bands = make_bitflags!(EmtcBand::{B1 | B2 | B8});` or
/// `let bands = EmtcBand::all();`
// See below for implementation notes
#[bitflags]
#[repr(u64)]
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum EmtcBand {
    B1 = 0x1,
    B2 = 0x2,
    B3 = 0x4,
    B4 = 0x8,
    B5 = 0x10,
    B8 = 0x80,
    B12 = 0x800,
    B13 = 0x1000,
    B18 = 0x20000,
    B19 = 0x40000,
    B20 = 0x80000,
    B25 = 0x1000000,
    B26 = 0x2000000, // no nbiot
    B27 = 0x4000000, // no nbiot
    B28 = 0x8000000,
    B66 = 0x200000000,      //= 0x20000000000000000,
    B85 = 0x10000000000000, //= 0x1000000000000000000000,
}

/// An NB-IoT radio band
///
/// Can be used as a [`BitFlags<NbiotBand>`] from the [`enumflags2`] crate to specify a list of
/// enabled radio bands, e.g. `let bands = make_bitflags!(NbiotBand::{B1 | B2 | B8});` or
/// `let bands = NbiotBand::all();`
// See below for implementation notes
#[bitflags]
#[repr(u64)]
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum NbiotBand {
    B1 = 0x1,
    B2 = 0x2,
    B3 = 0x4,
    B4 = 0x8,
    B5 = 0x10,
    B8 = 0x80,
    B12 = 0x800,
    B13 = 0x1000,
    B18 = 0x20000,
    B19 = 0x40000,
    B20 = 0x80000,
    B25 = 0x1000000,
    B28 = 0x8000000,
    B66 = 0x200000000,      //= 0x20000000000000000,
    B71 = 0x4000000000,     //= 0x400000000000000000, // no eMTC
    B85 = 0x10000000000000, //= 0x1000000000000000000000,
}

/// An LTE radio band
///
/// Can be used as a [`BitFlags<Band>`] from the [`enumflags2`] crate to specify a list of enabled
/// radio bands, e.g. `let bands = make_bitflags!(Band::{B1 | B2 | B8});` or `let bands =
/// Band::all();`
// As of 2021-10-05, repr(u128) is an unstable feature, see tracking issue at
// https://github.com/rust-lang/rust/issues/56071. Therefore, we have to considerably hack here :(
#[bitflags]
#[repr(u64)]
#[derive(Copy, Clone, Debug, PartialEq)]
enum Band {
    B1 = 0x1,
    B2 = 0x2,
    B3 = 0x4,
    B4 = 0x8,
    B5 = 0x10,
    B8 = 0x80,
    B12 = 0x800,
    B13 = 0x1000,
    B14 = 0x2000, // no nbiot or emtc
    B18 = 0x20000,
    B19 = 0x40000,
    B20 = 0x80000,
    B25 = 0x1000000,
    B26 = 0x2000000, // no nbiot
    B27 = 0x4000000, // no nbiot
    B28 = 0x8000000,
    B31 = 0x40000000, // no nbiot or emtc
    // bits 33 .. 64 are unused, so we shift B66 .. B85 down by 32 bits. When formatting, they have
    // to be shifted up again.
    B66 = 0x200000000,      //= 0x20000000000000000,
    B71 = 0x4000000000,     //= 0x400000000000000000, // no emtc
    B72 = 0x8000000000,     //= 0x800000000000000000, // no nbiot or emtc
    B73 = 0x10000000000,    //= 0x1000000000000000000, // no nbiot or emtc
    B85 = 0x10000000000000, //= 0x1000000000000000000000,
}

fn from_emtc(band: BitFlags<EmtcBand>) -> BitFlags<Band> {
    BitFlags::from_bits(band.bits())
        .expect("EmtcBand should only contain a subset of a generic Band")
}
fn from_nbiot(band: BitFlags<NbiotBand>) -> BitFlags<Band> {
    BitFlags::from_bits(band.bits())
        .expect("NbiotBand should only contain a subset of a generic Band")
}

/// Formats the given band info to be usable with the AT+QCFG="band" command
fn format_bands(bands: BitFlags<Band>) -> String<22> {
    let upper_bands_shifted = (bands.bits() & 0xFFFFFFFF00000000) as u128;
    let upper_bands = upper_bands_shifted << 32;
    let lower_bands = (bands.bits() & 0xFFFFFFFF) as u128;
    let all_bands = upper_bands | lower_bands;
    hl_format!("{:X}", all_bands).expect("Could not format radio bands")
}

/// Implements AT command "flows", i.e. a sequence of AT commands achieving a specific purpose
///
/// The methods should only be called if the modem is in the right state, i.e. this should be
/// wrapped by a type taking care of the internal modem state.
pub struct Bg77CmdFlows<'a, P0, P1, P2, TX, RX, UE, CLOCK>
where
    P0: embedded_hal::digital::OutputPin,
    P1: embedded_hal::digital::OutputPin,
    P2: embedded_hal::digital::OutputPin,
    UE: core::fmt::Debug,
    TX: embedded_hal_nb::serial::Write<u8, Error = UE>,
    RX: embedded_hal_nb::serial::Read<u8, Error = UE>,
    CLOCK: Clock,
{
    pin_enable: P0,
    pin_reset_n: P1,
    pin_pwrkey: P2,
    timer: Timer<'a, CLOCK>,
    at: atst_commands::AtHelper<'a, TX, RX, UE, CLOCK>,
    radio_config: RadioConfig,
    apn: Option<String<32>>,
    operator: Option<String<8>>,
    connection_timeout: Duration,
    delay_before_attach: Duration,
}

impl<'a, P0, P1, P2, TX, RX, UE, CLOCK> Bg77CmdFlows<'a, P0, P1, P2, TX, RX, UE, CLOCK>
where
    P0: embedded_hal::digital::OutputPin,
    P1: embedded_hal::digital::OutputPin,
    P2: embedded_hal::digital::OutputPin,
    UE: core::fmt::Debug,
    TX: embedded_hal_nb::serial::Write<u8, Error = UE>,
    RX: embedded_hal_nb::serial::Read<u8, Error = UE>,
    CLOCK: Clock,
{
    /// Creates a new Bg77CmdFlows instance
    ///
    /// `delay_before_attach` is a `Duration` that will be waited before the `AT+COPS` command
    /// is run to connect/attach to the network. This has been introduced because we have
    /// experienced connection issues on a low-power device when doing this too fast. This delay
    /// helped but could/should be avoided on devices where it is not necessary.
    pub fn new(
        hal: Bg77Hal<'a, P0, P1, P2, TX, RX, CLOCK>,
        radio_config: RadioConfig,
        apn: Option<&str>,
        operator: Option<&str>,
        connection_timeout: Duration,
        delay_before_attach: Duration,
        tx_rx_cooldown: Duration,
    ) -> Self {
        Bg77CmdFlows {
            pin_enable: hal.pin_enable,
            pin_reset_n: hal.pin_reset_n,
            pin_pwrkey: hal.pin_pwrkey,
            timer: Timer::new(hal.clock),
            at: atst_commands::AtHelper::new(hal.tx, hal.rx, hal.clock, tx_rx_cooldown),
            radio_config,
            apn: apn.map(|a| a.into()),
            operator: operator.map(|o| o.into()),
            connection_timeout,
            delay_before_attach,
        }
    }

    #[cfg(feature = "direct-serial-access")]
    pub fn serial(&mut self) -> (&mut TX, &mut RX) {
        self.at.serial()
    }

    /// Power-off, wait, power-on, wait, reset (+pwrkey_n), wait, un-reset
    pub fn power_on(&mut self) -> Result<(), Bg77Error> {
        log::info!("Turn BG77 modem on");

        // Here, we want to do a reset / power cycle to ensure that the
        // modem boots up even if it was powered before.

        #[cfg(feature = "bg77")]
        {
            // From Quectel_BG77_Hardware_Design_V1.0.pdf:
            // "RESET_N is used to reset the module. Due to platform limitations,
            // the chipset has integrated the reset function into PWRKEY, and
            // RESET_N is connected directly to PWRKEY inside the module.
            // The module can be reset by driving RESET_N low for 2-3.8 s."
            //
            // Due to these limitations, a reset using the RESET_N pin takes
            // a huge amount of time. Thus, we rather power cycle the modem
            // completely which is much faster.
            self.pin_enable.set_low().map_err(|_| Bg77Error::Hardware)?;
            wait_ms(&mut self.timer, 100);
            self.pin_enable
                .set_high()
                .map_err(|_| Bg77Error::Hardware)?;
            wait_ms(&mut self.timer, 100);
            self.pin_reset_n
                .set_low()
                .map_err(|_| Bg77Error::Hardware)?;
            self.pin_pwrkey.set_low().map_err(|_| Bg77Error::Hardware)?;
            wait_ms(&mut self.timer, 550);
            self.pin_reset_n
                .set_high()
                .map_err(|_| Bg77Error::Hardware)?;
            self.pin_pwrkey
                .set_high()
                .map_err(|_| Bg77Error::Hardware)?;
        }

        #[cfg(feature = "bg770")]
        {
            // In our tests, VDD_EXT (which powered our uart level shifters) went low after a few
            // hundred ms. We could workaround this with a subsequent modem reset.
            self.pin_enable.set_low().map_err(|_| Bg77Error::Hardware)?;
            wait_ms(&mut self.timer, 100);
            self.pin_enable
                .set_high()
                .map_err(|_| Bg77Error::Hardware)?;
            wait_ms(&mut self.timer, 500);

            self.pin_reset_n.set_low().ok();
            wait_ms(&mut self.timer, 100);
            self.pin_reset_n.set_high().ok();
            wait_ms(&mut self.timer, 100);

            self.pin_pwrkey.set_low().ok();
            wait_ms(&mut self.timer, 550);
            self.pin_pwrkey.set_high().ok();
            wait_ms(&mut self.timer, 100);
        }

        log::info!("BG77 modem turned on");
        Ok(())
    }

    pub fn power_off(&mut self) {
        let _ = self.pin_enable.set_low();
    }

    /// Configure the modem as requested and try to attach
    pub fn connect_network(&mut self) -> Result<(), Bg77Error> {
        log::info!("Connect to network");
        self.uart_ready_at_command(10)?;

        log::debug!("DisableSleep");
        self.at.simple_command(b"AT+QSCLK=0\r")?;

        log::debug!("EnableEcho");
        self.at.simple_command(b"AT+CMEE=1\r")?;

        log::debug!("Check SIM & GetPinState");
        let res = self
            .at
            .command_with_response(b"AT+CPIN?\r", &[b"+CPIN: ", b"\r\n\r\nOK\r\n"])?;
        if res[0] != b"READY" {
            return Err(Bg77Error::PinRequired);
        }
        drop(res);

        log::debug!("Turn modem functionality on");
        #[cfg(feature = "bg77")]
        self.at.simple_command(b"AT+CFUN=1,0\r")?; // fast on BG77
        #[cfg(feature = "bg770")]
        self.at
            .command_with_timeout(b"AT+CFUN=1,0\r", Duration::from_secs(5))?; // slower on BG770

        // Ensure that the modem does not auto-connect
        log::debug!("SelectOperator (detach)");
        self.at
            .command_with_timeout(b"AT+COPS=2\r", Duration::from_secs(10))?;

        let (iotopmode, scanseq, emtc_bands, nbiot_bands, cops_act);
        match self.radio_config {
            RadioConfig::Both {
                emtc_bands: emtc_bnds,
                nbiot_bands: nbiot_bnds,
            } => {
                iotopmode = 2;
                scanseq = "00";
                emtc_bands = format_bands(from_emtc(emtc_bnds));
                nbiot_bands = format_bands(from_nbiot(nbiot_bnds));
                cops_act = None;
            }
            RadioConfig::OnlyEmtc { bands } => {
                iotopmode = 0;
                // Prevent "error[E0658]: attributes on expressions are experimental" by { ... }
                #[cfg(feature = "bg77")]
                {
                    scanseq = "02"; // ok on BG77
                }
                #[cfg(feature = "bg770")]
                {
                    scanseq = "0203"; // both required on BG770
                }
                emtc_bands = format_bands(from_emtc(bands));
                nbiot_bands = String::from("0");
                cops_act = Some(8);
            }
            RadioConfig::OnlyNbiot { bands } => {
                iotopmode = 1;
                #[cfg(feature = "bg77")]
                {
                    scanseq = "03"; // ok on BG77
                }
                #[cfg(feature = "bg770")]
                {
                    scanseq = "0302"; // both required on BG770
                }
                emtc_bands = String::from("0");
                nbiot_bands = format_bands(from_nbiot(bands));
                cops_act = Some(9);
            }
            RadioConfig::Prioritized {
                preference: RadioTechnology::eMTC,
                emtc_bands: emtc_bnds,
                nbiot_bands: nbiot_bnds,
            } => {
                iotopmode = 2;
                scanseq = "0203";
                emtc_bands = format_bands(from_emtc(emtc_bnds));
                nbiot_bands = format_bands(from_nbiot(nbiot_bnds));
                cops_act = None;
            }
            RadioConfig::Prioritized {
                preference: RadioTechnology::NB_IoT,
                emtc_bands: emtc_bnds,
                nbiot_bands: nbiot_bnds,
            } => {
                iotopmode = 2;
                scanseq = "0302";
                emtc_bands = format_bands(from_emtc(emtc_bnds));
                nbiot_bands = format_bands(from_nbiot(nbiot_bnds));
                cops_act = None;
            }
        }

        // For the QT+QCFG=... commands: The last parameter is <effect>: 0 = after reboot, 1 = now

        // Servicedomain 1 = PS only = only packet switched transmissions, no voice connection
        log::debug!("Configure servicedomain");
        #[cfg(feature = "bg77")]
        self.at.simple_command(b"AT+QCFG=\"servicedomain\",1,1\r")?; // BG77
        #[cfg(feature = "bg770")]
        self.at.simple_command(b"AT+QCFG=\"servicedomain\",1\r")?; // effect parameter missing on BG770

        // Iotopmode: 0 = eMTC, 1 = NB-IoT, 2 = both
        log::debug!("Configure iotopmode");
        let cmd: String<64> = hl_format!("AT+QCFG=\"iotopmode\",{},1\r", iotopmode)?;
        #[cfg(feature = "bg77")]
        self.at.simple_command(cmd.as_ref())?; // fast on BG77
        #[cfg(feature = "bg770")]
        self.at
            .command_with_timeout(cmd.as_ref(), Duration::from_secs(6))?; // takes 4s on BG770

        // Scansequence: 00 = automatic, 01 = GSM, 02 = eMTC, 03 = NB-IoT
        // e.g. 0203 = eMTC first, NB-IoT second
        log::debug!("Configure scansequence");
        let cmd: String<64> = hl_format!("AT+QCFG=\"nwscanseq\",{},1\r", scanseq)?;
        self.at.simple_command(cmd.as_ref())?;

        // AT+QCFG="band", <gsm>, <eMTC>, <NB-IoT>, <effect>
        // For each protocol: 0 = no change
        // Otherwise, it is a hexadecimal bitmask with the following meanings. It has to be
        // transmitted without the "0x" prepended, e.g. 'AT+QCFG="band",0,0,15' is
        // 0x01 | 0x04 | 0x10 and chooses B1, B3, B5 for NB-IoT
        //
        // The following table is taken from the QCFG AT Commands Manuals for BG77 and BG770. It
        // has been created by merging the eMTC and NB-IoT listings for both modems. The additional
        // notes on the right designate if a specific band was only found in specific places.
        // In general, band N is activated by setting bit N to 1.
        //
        // 0x1 (BAND_PREF_LTE_BAND1)                        LTE B1
        // 0x2 (BAND_PREF_LTE_BAND2)                        LTE B2
        // 0x4 (BAND_PREF_LTE_BAND3)                        LTE B3
        // 0x8 (BAND_PREF_LTE_BAND4)                        LTE B4
        // 0x10 (BAND_PREF_LTE_BAND5)                       LTE B5
        // 0x80 (BAND_PREF_LTE_BAND8)                       LTE B8
        // 0x800 (BAND_PREF_LTE_BAND12)                     LTE B12
        // 0x1000 (BAND_PREF_LTE_BAND13)                    LTE B13
        // 0x10000 (BAND_PREF_LTE_BAND17)                   LTE B17 <-- only BG770 NB-IoT?
        // 0x20000 (BAND_PREF_LTE_BAND18)                   LTE B18
        // 0x40000 (BAND_PREF_LTE_BAND19)                   LTE B19
        // 0x80000 (BAND_PREF_LTE_BAND20)                   LTE B20
        // 0x1000000 (BAND_PREF_LTE_BAND25)                 LTE B25
        // 0x2000000 (BAND_PREF_LTE_BAND26)                 LTE B26 <-- not BG770 NB-IoT?
        // 0x4000000 (BAND_PREF_LTE_BAND27)                 LTE B27 <-- not BG770 NB-IoT?
        // 0x8000000 (BAND_PREF_LTE_BAND28)                 LTE B28
        // 0x40000000 (BAND_PREF_LTE_BAND31)                LTE B31 <-- only BG77?
        // 0x20000000000000000 (BAND_PREF_LTE_BAND66)       LTE B66
        // 0x400000000000000000 (BAND_PREF_LTE_BAND71)      LTE B71 <-- only BG77 NB-IoT?
        // 0x800000000000000000 (BAND_PREF_LTE_BAND72       LTE B72 <-- only BG77?
        // 0x1000000000000000000 (BAND_PREF_LTE_BAND73)     LTE B73 <-- only BG77?
        // 0x1000000000000000000000 (BAND_PREF_LTE_BAND85)  LTE B85 <-- only BG77?
        log::debug!("Configure band");
        let cmd: String<65> = hl_format!("AT+QCFG=\"band\",0,{},{},1\r", emtc_bands, nbiot_bands)?;
        #[cfg(feature = "bg77")]
        self.at.simple_command(cmd.as_ref())?; // fast on BG77
        #[cfg(feature = "bg770")]
        self.at
            .command_with_timeout(cmd.as_ref(), Duration::from_secs(4))?; // takes up to 2s on BG770

        // AT+CGDCONT=<context-id>,<PDP-type>,<APN>
        // context-id: minimum 1, can be read with the test form of the command
        // pdp-type: IP = IPv4, other options are PPP, IPV6, IPV4V6
        // apn: "if the value is null or omitted, then the subscription value will be requested"
        log::debug!("Set pdp context");
        let cmd: String<64> = match &self.apn {
            Some(apn) => hl_format!("AT+CGDCONT=1,\"IP\",\"{}\"\r", apn)?,
            None => String::from("AT+CGDCONT=1,\"IP\"\r"),
        };
        self.at.simple_command(cmd.as_ref())?;

        if self.delay_before_attach != Duration::new(0, 0) {
            log::debug!("Wait before attach");
            wait(&mut self.timer, self.delay_before_attach);
        }

        // AT+COPS=<mode>,[<format>,<operator>,[<access-technology>]]
        // mode: 0 = auto, 1 = manual, 2 = deregister, 3 = only config, 4 = try manual, then auto
        // format: 2 = numeric format = GSM location are id number, e.g. 26201 German Telekom
        // access-technology: 0 = GSM, 8 = eMTC, 9 = NB-IoT
        // examples: AT+COPS=0    AT+COPS=1,2,"26201"    AT+COPS=1,2,"26201",9
        log::debug!("Select operator");
        let cmd: String<64> = match (&self.operator, cops_act) {
            (Some(operator), Some(act)) => hl_format!("AT+COPS=1,2,\"{}\",{}\r", operator, act)?,
            (Some(operator), None) => hl_format!("AT+COPS=1,2,\"{}\"\r", operator)?,
            (None, _) => String::from("AT+COPS=0\r"),
        };
        self.at
            .command_with_timeout(cmd.as_ref(), self.connection_timeout)?;

        log::debug!("Check attachment state");
        // TODO Instead of looping timeout.as_secs() times, use a proper timeout for the whole
        // flow. In the current setup, this would require another embedded_hal::timer::CountDown so
        // it was not done in the beginning. Maybe, switch to a clock abstraction instead of
        // CountDown?
        for _ in 0..=self.connection_timeout.as_secs() {
            let res = self
                .at
                .command_with_response(b"AT+CGATT?\r", &[b"+CGATT: ", b"\r\n\r\nOK\r\n"])?;
            if res[0] == b"1" {
                drop(res);
                log::debug!("Disable data echo");
                self.at.simple_command(b"AT+QISDE=0\r")?;

                return Ok(());
            }
            wait_ms(&mut self.timer, 1000);
        }

        Err(Bg77Error::CgattFailure)
    }

    /// Tries `num_tries` times to communicate with the modem ("AT\r")
    fn uart_ready_at_command(&mut self, num_tries: usize) -> Result<(), Bg77Error> {
        for _ in 0..num_tries {
            log::debug!("Modem ready? Sending AT!");
            if self.at.simple_command(b"AT\r").is_ok() {
                return Ok(());
            }
        }
        Err(Bg77Error::AtCmdTimeout)
    }

    /// Detach from the network
    pub fn disconnect_network(&mut self) -> Result<(), Bg77Error> {
        log::info!("Disconnect from network / Power off");
        log::debug!("SelectOperator (detach)");
        self.at
            .command_with_timeout(b"AT+COPS=2\r", Duration::from_secs(10))
    }

    pub fn open_socket(
        &mut self,
        socket_index: usize,
        protocol: Protocol,
        remote: net::SocketAddr,
    ) -> Result<(), Bg77Error> {
        let protocol = match protocol {
            Protocol::Tcp => "TCP",
            Protocol::Udp => "UDP",
        };
        let ip_address = match remote {
            net::SocketAddr::V6(_) => {
                log::warn!("IPv6 addresses are not supported (yet?)");
                return Err(Bg77Error::IPv6Unsupported);
            }
            net::SocketAddr::V4(v4) => {
                let ip_address: String<16> = hl_format!("{}", v4.ip())?;
                ip_address
            }
        };
        let local_port = 0; // = automatic
        log::info!("Open {} socket", protocol);
        let cmd: String<64> = hl_format!(
            "AT+QIOPEN=1,{},\"{}\",\"{}\",{},{}\r",
            socket_index,
            protocol,
            ip_address,
            remote.port(),
            local_port
        )?;
        // Bg77 TCP/IP Application Note:
        // "3. It is suggested to wait for 150 seconds for “+QIOPEN: <connectID>,<err>” to be
        // outputted after executing the Write Command. If the URC cannot be received in 150
        // seconds, AT+QICLOSE should be used to close the socket."
        if let Ok(res) = self.at.command(
            cmd.as_ref(),
            &[b"\r\nOK\r\n\r\n+QIOPEN: ", b",", b"\r\n"],
            Duration::from_secs(10), // no, we are not that patient ...
        ) {
            if let Ok(Ok(socket_id)) = core::str::from_utf8(res[0]).map(str::parse::<usize>) {
                if socket_id == socket_index && res[1] == b"0" {
                    // socket_id , err
                    return Ok(());
                }
            }
        }
        log::warn!("Could not open socket, calling 'close_socket' to clean up.");
        let _ = self.close_socket(socket_index);
        Err(Bg77Error::OpenSocketError)
    }

    pub fn close_socket(&mut self, socket_index: usize) -> Result<(), Bg77Error> {
        log::info!("Closing socket");
        #[cfg(feature = "bg77")]
        let cmd: String<64> = hl_format!("AT+QICLOSE={},10\r", socket_index)?; // BG77
        #[cfg(feature = "bg770")]
        let cmd: String<64> = hl_format!("AT+QICLOSE={}\r", socket_index)?; // with timeout -> ERROR on BG770
        self.at
            .command_with_timeout(cmd.as_ref(), Duration::from_secs(11))
    }

    pub fn send_data(
        &mut self,
        socket_index: usize,
        protocol: Protocol,
        tx_data: &[u8],
    ) -> Result<usize, Bg77Error> {
        log::info!("Send data");
        log::debug!("Announce data transmission");
        // For TCP, we could automatically transmit the data in multiple segments. But the trait
        // definition actually allows to just transmit part of the data so any user of an
        // embedded_nal::TcpClientStack should be prepared to handle partial transmits. So if we
        // did transmit multiple segments here, this should lead to code duplication, so we don't.
        let txlen = tx_data.len();
        let txlen = match (txlen > 1460, protocol) {
            (true, Protocol::Udp) => return Err(Bg77Error::TxSize),
            (true, Protocol::Tcp) => 1460,
            (false, _) => txlen,
        };
        let cmd: String<64> = hl_format!("AT+QISEND={},{}\r", socket_index, txlen)?;
        self.at.command_with_response(cmd.as_ref(), &[b"> "])?;
        log::debug!("Transmit raw data");
        self.at
            .command_with_response(&tx_data[..txlen], &[b"\r\nSEND OK\r\n"])?;
        Ok(txlen)
    }

    pub fn receive_data(
        &mut self,
        socket_index: usize,
        buffer: &mut [u8],
    ) -> nb::Result<usize, Bg77Error> {
        log::info!("Read data");
        log::debug!("Sending read data command.");
        let max_data_length = core::cmp::min(1460, buffer.len());
        let cmd: String<64> = hl_format!("AT+QIRD={},{}\r", socket_index, max_data_length)?;
        let (res, parsed_len) = self.at.command_with_parsed_len(
            cmd.as_ref(),
            &[b"+QIRD: ", b"\r\n"],
            Duration::from_secs(1),
        )?;
        let data_length_str = core::str::from_utf8(res[0]).map_err(|_| Bg77Error::ParseFailed)?;
        let data_length: usize = data_length_str
            .parse()
            .map_err(|_| Bg77Error::ParseFailed)?;
        drop(res);
        if data_length > max_data_length {
            log::error!("Modem responded with more data than we requested!");
            return Err(nb::Error::Other(Bg77Error::RxSize));
        }
        if data_length == 0 {
            Err(nb::Error::WouldBlock)
        } else {
            log::debug!("Read raw data");
            self.at
                .read_raw_data(&mut buffer[..data_length], 2500, parsed_len)?;
            Ok(data_length)
        }
    }

    pub fn dns_lookup(&mut self, hostname: &str) -> Result<net::IpAddr, Bg77Error> {
        log::info!("DNS lookup");
        let cmd: String<64> = hl_format!("AT+QIDNSGIP=1,\"{}\"\r", hostname)?;
        let res = self.at.command(
            cmd.as_ref(),
            // OK\r\n\r\n+QIURC: "dnsgip",0(error),<ip_count>,<dns_ttl>\r\n+QIURC: "dnsgip","<ip_address>"
            &[
                b"\r\nOK\r\n\r\n+QIURC: \"dnsgip\",0,", // IP_count   -> res[0]
                b",",                                   // DNS_ttl    -> res[1]
                b"\r\n\r\n+QIURC: \"dnsgip\",\"",       // IP address -> res[2]
                b"\"\r\n",
            ],
            Duration::from_secs(10),
        )?;
        let ip_str = core::str::from_utf8(res[2]).map_err(|_| Bg77Error::ParseIpAddressFailed)?;
        let mut ip_addr_parts = [0; 4];
        for (i, octet) in ip_str.split('.').enumerate() {
            if i >= 4 {
                return Err(Bg77Error::ParseIpAddressFailed);
            }
            ip_addr_parts[i] = octet.parse().map_err(|_| Bg77Error::ParseIpAddressFailed)?;
        }
        let ap = ip_addr_parts;
        let ipv4 = net::Ipv4Addr::new(ap[0], ap[1], ap[2], ap[3]);
        Ok(net::IpAddr::V4(ipv4))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use enumflags2::BitFlag;
    #[test]
    fn band_formatting() {
        assert_eq!(&format_bands(Band::B1.into()), "1");
        assert_eq!(&format_bands(Band::B8.into()), "80");
        assert_eq!(&format_bands(Band::B1 | Band::B8), "81");
        assert_eq!(&format_bands(Band::B1 | Band::B2), "3");
        assert_eq!(&format_bands(Band::B28 | Band::B2), "8000002");
        assert_eq!(&format_bands(Band::B66.into()), "20000000000000000");
        assert_eq!(&format_bands(Band::B66 | Band::B1), "20000000000000001");
        assert_eq!(&format_bands(Band::B72.into()), "800000000000000000");
        assert_eq!(&format_bands(Band::B73.into()), "1000000000000000000");
        assert_eq!(&format_bands(Band::B85.into()), "1000000000000000000000");
        assert_eq!(
            &format_bands(Band::B66 | Band::B71 | Band::B72 | Band::B73 | Band::B85),
            "1001C20000000000000000"
        );
        assert_eq!(&format_bands(Band::all()), "1001C2000000004F0E389F");
    }
}
