// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

use core::time::Duration;
use embedded_hal_nb::serial::{Read, Write};
use embedded_timers::{clock::Clock, timer::Timer};
use heapless::Vec;
use try_ascii::try_ascii;

use crate::{error::Bg77Error, util::*};

/// Helper type to assist in running AT commands, especially in parsing the responses
pub struct AtHelper<'a, TX, RX, E, CLOCK>
where
    TX: Write<u8, Error = E>,
    RX: Read<u8, Error = E>,
    CLOCK: Clock,
{
    tx: TX,
    rx: RX,
    timer: Timer<'a, CLOCK>,
    tx_rx_cooldown: Duration,
    input: Vec<u8, 2048>, // TODO generic size
}

impl<'a, TX, RX, E, CLOCK> AtHelper<'a, TX, RX, E, CLOCK>
where
    E: core::fmt::Debug,
    TX: Write<u8, Error = E>,
    RX: Read<u8, Error = E>,
    CLOCK: Clock,
{
    pub fn new(tx: TX, rx: RX, clock: &'a CLOCK, tx_rx_cooldown: Duration) -> Self {
        AtHelper {
            tx,
            rx,
            timer: Timer::new(clock),
            tx_rx_cooldown,
            input: Vec::new(),
        }
    }

    #[cfg(feature = "direct-serial-access")]
    pub fn serial(&mut self) -> (&mut TX, &mut RX) {
        (&mut self.tx, &mut self.rx)
    }

    /// Reads all available rx data into the internal self.input: heapless::Vec
    fn read_all_rx_data(&mut self) -> Result<(), Bg77Error> {
        let oldlen = self.input.len();
        // TODO It looks like this loop is quite slow so that we are forced
        // to use a huge input buffer which is able to hold the whole
        // "receive data" (worst case) message just in case there is a lot
        // of data to be read. Maybe, this could be replaced with a more low
        // level (and hopefully faster) approach so that we can just read the
        // beginning of the answer, extract the data length and then just
        // read the raw data into the target buffer.
        // TODO Nicer error handling for rx errors, e.g. Overflows?

        loop {
            match self.rx.read() {
                Ok(byte) => {
                    self.input.push(byte).map_err(|_| {
                        log::debug!("RxBytes {:?}", try_ascii(&self.input[oldlen..]));
                        log::warn!("Input buffer overflow. Use a bigger input buffer to prevent this kind of error.");
                        Bg77Error::BufferOverflow
                    })?;
                }
                Err(nb::Error::WouldBlock) => break,
                Err(nb::Error::Other(e)) => {
                    log::error!("rx read error: {:?}", e);
                    break;
                }
            }
        }

        if self.input.len() != oldlen {
            log::debug!("RxBytes {:?}", try_ascii(&self.input[oldlen..]));
        }
        Ok(())
    }

    /// Clears the internal self.input: heapless::Vec and removes all available data from self.rx
    fn clear_all_input(&mut self) {
        self.input.clear();
        loop {
            if let Err(nb::Error::WouldBlock) = self.rx.read() {
                break;
            }
        }
    }

    /// Removes the specified amount of data from the internal self.input: heapless::Vec
    fn consume_input_data(&mut self, len: usize) {
        if len > self.input.len() {
            self.input.clear();
            return;
        }
        for i in 0..self.input.len() - len {
            self.input[i] = self.input[i + len];
        }
        self.input.truncate(self.input.len() - len);
    }

    /// Finds `needle` in self.input starting the search at `offset`, returns the position where
    /// `needle` starts
    fn find_pos_in_input(&self, offset: usize, needle: &[u8]) -> nb::Result<usize, Bg77Error> {
        self.input[offset..]
            .windows(needle.len())
            .position(|window| window == needle)
            .ok_or(nb::Error::WouldBlock)
            .map(|pos| pos + offset)
    }

    /// Checks if the given response format `resp_parts` can be found in `self.input`. Does not
    /// read from `self.rx`, this has to be done separately before.
    ///
    /// The response format is given as a list of substrings that should be found in the response.
    /// For example, if the response to "AT+CGATT?\r" looks like "+CGATT: <attached>\r\n\r\nOK\r\n",
    /// you will most probably find the <attached> state that tells you if you are successfully
    /// attached. So you split the response in the two substrings "+CGATT: " and "\r\n\r\nOK\r\n"
    /// and your `resp_parts` look like &[b"+CGATT: ", b"\r\n\r\nOK\r\n"].
    ///
    /// Returns a `heapless::Vec` of (start, end) pairs which can be used to extract the values
    /// from the response, i.e. `self.input[start..end]`. If `resp_parts` contains P parts, the
    /// returned `Vec` will be (P-1) items long. Working rather low-level here, it was decided not
    /// to return references into `self.input` directly because this would cause a lot of borrow
    /// checker lifetime headache here and when calling the method.
    fn check_response(
        &mut self,
        resp_parts: &[&[u8]],
    ) -> nb::Result<Vec<(usize, usize), 8>, Bg77Error> {
        let mut out = Vec::new();
        if resp_parts.is_empty() {
            return Ok(out);
        }
        let mut start = self.find_pos_in_input(0, resp_parts[0])? + resp_parts[0].len();
        for part in &resp_parts[1..] {
            let pos = self.find_pos_in_input(start + 1, part)?;
            out.push((start, pos)).map_err(|_| {
                log::warn!("Output buffer overflow. Use a bigger buffer size.");
                nb::Error::Other(Bg77Error::BufferOverflow)
            })?;
            start = pos + part.len();
        }
        Ok(out)
    }

    /// Throws away all previous buffered input, transmits the given command and waits for the
    /// given response or the two general "ERROR" and "+CME ERROR: ...\r\n" responses.
    ///
    /// Returns a tuple of response values and the total
    /// matched length. The response values are references into the input data which point at the
    /// sections between the `response_parts` (see `check_response` method). The total matched
    /// length is the number of bytes that were used to match the response format. This can be
    /// required as input argument for `read_raw_data`, used for "separated" AT commands.
    ///
    /// This is the most general command method but in most cases not the one that is required.
    pub fn command_with_parsed_len<'out>(
        &'out mut self,
        cmd: &[u8],
        response_parts: &[&[u8]],
        timeout: Duration,
    ) -> Result<(Vec<&'out [u8], 8>, usize), Bg77Error> {
        if self.tx_rx_cooldown != Duration::new(0, 0) {
            wait(&mut self.timer, self.tx_rx_cooldown);
        }

        // TODO generic size
        // Throw away all previous input data before transmitting the command.
        // This approach is ok for simple command-response flows but will be
        // problematic if true URCs should be handled.
        // If this was not done here, our input buffer would be full at some
        // point so then another approach to avoid overflows would be required.
        self.clear_all_input();
        // send command
        for &c in cmd {
            nb::block!(self.tx.write(c)).map_err(|_| Bg77Error::Hardware)?;
        }

        if self.tx_rx_cooldown != Duration::new(0, 0) {
            wait(&mut self.timer, self.tx_rx_cooldown);
        }

        // wait for the response until timeout
        self.timer.start(timeout);
        loop {
            self.read_all_rx_data()?;
            match self.check_response(response_parts) {
                Ok(resp) => {
                    let mut out = Vec::new();
                    let mut last_part_start = 0;
                    for (start, end) in resp {
                        out.push(&self.input[start..end])
                            .expect("Output vector sizes should be the same.");
                        last_part_start = end;
                    }
                    let last_part_len = if !response_parts.is_empty() {
                        response_parts[response_parts.len() - 1].len()
                    } else {
                        0
                    };
                    let parsed_len = last_part_start + last_part_len;
                    return Ok((out, parsed_len));
                }
                Err(nb::Error::WouldBlock) => {}
                Err(nb::Error::Other(e)) => return Err(e),
            };
            match self.check_response(&[b"ERROR"]) {
                Ok(_) => return Err(Bg77Error::AtCmdError),
                Err(nb::Error::WouldBlock) => {}
                Err(nb::Error::Other(e)) => return Err(e),
            }
            match self.check_response(&[b"+CME ERROR:", b"\r\n"]) {
                Ok(_) => return Err(Bg77Error::AtCmdCmeError),
                Err(nb::Error::WouldBlock) => {}
                Err(nb::Error::Other(e)) => return Err(e),
            }
            if self.timer.wait() == Ok(()) {
                return Err(Bg77Error::AtCmdTimeout);
            }
        }
    }

    pub fn command<'out>(
        &'out mut self,
        cmd: &[u8],
        response_parts: &[&[u8]],
        timeout: Duration,
    ) -> Result<Vec<&'out [u8], 8>, Bg77Error> {
        self.command_with_parsed_len(cmd, response_parts, timeout)
            .map(|(res, _len)| res)
    }

    pub fn simple_command(&mut self, cmd: &[u8]) -> Result<(), Bg77Error> {
        self.command(cmd, &[b"\r\nOK\r\n"], Duration::from_secs(1))
            .map(|_| ())
    }

    pub fn command_with_response<'out>(
        &'out mut self,
        cmd: &[u8],
        response_parts: &[&[u8]],
    ) -> Result<Vec<&'out [u8], 8>, Bg77Error> {
        self.command(cmd, response_parts, Duration::from_secs(1))
    }

    pub fn command_with_timeout(&mut self, cmd: &[u8], timeout: Duration) -> Result<(), Bg77Error> {
        self.command(cmd, &[b"\r\nOK\r\n"], timeout).map(|_| ())
    }

    /// Removes (at most) `consume_before` from `self.input` and fills `buffer` with input data
    /// afterwards
    pub fn read_raw_data(
        &mut self,
        mut buffer: &mut [u8],
        timeout_ms: u32,
        consume_before: usize,
    ) -> Result<(), Bg77Error> {
        self.consume_input_data(consume_before);
        self.timer.start(Duration::from_millis(timeout_ms as u64));
        loop {
            self.read_all_rx_data()?;
            let available_len = core::cmp::min(self.input.len(), buffer.len());
            buffer[..available_len].copy_from_slice(&self.input[..available_len]);
            self.consume_input_data(available_len);
            let (_, new_buffer) = buffer.split_at_mut(available_len);
            buffer = new_buffer;
            if buffer.is_empty() {
                return Ok(());
            }
            if self.timer.wait() == Ok(()) {
                return Err(Bg77Error::AtCmdTimeout);
            }
        }
    }
}
